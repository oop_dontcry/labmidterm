public class PlayGenshin {
    public static void main(String[] args) {
        GenshinImpact character1 = new GenshinImpact(); // Create Object
        character1.setName("Yae");
        character1.setElement("Electro");
        character1.setRegion("Inazuma");
        character1.displayCharacter();
        character1.Run();
        character1.ChargeAttack();
        character1.Overloaded();

        GenshinImpact character2 = new GenshinImpact();
        character2.setName("Kazuha");
        character2.setElement("Anemo");
        character2.setRegion("Inazuma");
        character2.displayCharacter();
        character2.Run();
        character2.PressE();
        character2.SwirlPyro();

        GenshinImpact character3 = new GenshinImpact();
        character3.setName("Kokomi");
        character3.setElement("Hydro");
        character3.setRegion("Inazuma");
        character3.displayCharacter();
        character3.Run();
        character3.PressE();
        character3.Heal();
    }
}
