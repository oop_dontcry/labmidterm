public class GenshinImpact {
    // Attribute
    private String Name;
    private String Element;
    private String Region;

    public GenshinImpact() {
        System.out.println("----Create Object Complete----");
    }
    // Method
    public void Run() {
        System.out.println(Name+" is running");
    }

    public void Overloaded() {
        System.out.println("Overload Activate");
    }

    public void ChargeAttack() {
        System.out.println(Name+" use a charge attack");
    }
    public void SwirlPyro() {
        System.out.println("Swirl Activate with pyro");
    }
    public void Heal() {
        System.out.println(Name+" heal Hp to all allies");
    }
    public void PressE() {
        System.out.println(Name+" activate skill");
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public void setElement(String Element) {
        this.Element = Element;
    }

    public void setRegion(String Region) {
        this.Region = Region;
    }

    public void displayCharacter() {
        System.out.println("Name = " + this.Name);
        System.out.println("Element = " + this.Element);
        System.out.println("Region = " + this.Region);
    }
}