public class PlayPokemon {
    public static void main(String[] args) {
        
        Pokemon p1 = new Pokemon();
        p1.setName("Pikachu");
        p1.setType("Electric");
        p1.setColor("Yellow");
        p1.displayPokemon();
        p1.Attack();
        p1.Dodge();
        p1.Skill();

        Pokemon p2 = new Pokemon();
        p2.setName("Wynaut");
        p2.setType("Psychic");
        p2.setColor("Blue");
        p2.displayPokemon();
        p2.Dodge();
        p2.Skill();
        p2.Evolve();

        Pokemon p3 = new Pokemon();
        p3.setName("Necrozma");
        p3.setType("Psychic");
        p3.setColor("Black");
        p3.displayPokemon();
        p3.Attack();
        p3.Skill();
        p3.Evolve();
    }
}
