public class Pokemon {
    // Attribute
    private String Name;
    private String Type;
    private String Color;

    public Pokemon() {
        System.out.println("----Create Object Complete----");
    }

    // Method
    public void setName(String Name) {
        this.Name = Name;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public void setColor(String Color) {
        this.Color = Color;
    }

    public void Attack() {
        System.out.println("I order "+Name+" to attack");
    }

    public void Dodge() {
        System.out.println("I order "+Name+" to dodge");
    }

    public void Evolve() {
        System.out.println(Name+"has EVOLVED!!!");
    }

    public void Skill() {
        System.out.println("I order "+Name+" to use skill");
    }

    public void displayPokemon() {
        System.out.println("Name = " + this.Name);
        System.out.println("Type = " + this.Type);
        System.out.println("Color = " + this.Color);
    }
}
